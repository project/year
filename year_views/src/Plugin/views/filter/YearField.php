<?php

namespace Drupal\year_views\Plugin\views\filter;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\ViewExecutable;
use Drupal\views\Plugin\views\display\DisplayPluginBase;
use Drupal\views\Plugin\views\filter\ManyToOne;

/**
 * Filter by year.
 *
 * @ingroup views_filter_handlers
 *
 * @ViewsFilter("year_field")
 */
class YearField extends ManyToOne {

  /**
   * {@inheritdoc}
   */
  public function init(ViewExecutable $view, DisplayPluginBase $display, array &$options = NULL) {
    parent::init($view, $display, $options);

    // Set the options based on extra settings.
    $range = range($this->calculateYear($this->options['year_from']), $this->calculateYear($this->options['year_to']));

    // Reverse the sort order if needed.
    if ($this->options['sort_order'] == 'desc') {
      $range = array_reverse($range, TRUE);
    }

    $this->valueOptions = array_combine($range, $range);
  }

  /**
   * Calculate a year value based on provide numeric or relative string.
   *
   * @param string $year
   *   String representation of a specific year or relative strtotime format.
   *
   * @return int
   *   The calculated year value as an integer.
   */
  protected function calculateYear(string $year) {
    if (!is_numeric($year)) {
      $year = date('Y', strtotime($year));
    }
    return $year;
  }

  /**
   * {@inheritdoc}
   */
  public function hasExtraOptions() {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function getValueOptions() {
    return $this->valueOptions;
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['year_from'] = ['default' => '-30 years'];
    $options['year_to'] = ['default' => '+15 years'];
    $options['sort_order'] = ['default' => 'asc'];

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildExtraOptionsForm(&$form, FormStateInterface $form_state) {

    $form['markup1'] = [
      '#type' => 'markup',
      '#markup' => $this->t("<p>Provide a range of years to display in the fields below.<br>
        Enter specific year values &mdash; <strong>1965</strong>, <strong>425</strong>, <strong>2055</strong>, etc.<br>
        <strong>- OR -</strong><br>
        Describe a <strong>time relative</strong> to the current date &mdash; <strong>now</strong> (current year), '<strong>-20 years</strong>' (20 years before the current date).
        See <a href=\"https://www.php.net/manual/datetime.formats.relative.php\">Relative formats</a> for details.</p>"),
      '#weight' => 1,
    ];

    $form['year_from'] = [
      '#type' => 'textfield',
      '#title' => $this->t('From year'),
      '#required' => TRUE,
      '#default_value' => $this->options['year_from'],
      '#description' => $this->t("Enter a starting year"),
      '#weight' => 2,
    ];

    $form['year_to'] = [
      '#type' => 'textfield',
      '#title' => $this->t('To year'),
      '#required' => TRUE,
      '#default_value' => $this->options['year_to'],
      '#description' => $this->t("Enter an end year."),
      '#weight' => 2,
    ];

    $form['sort_order'] = [
      '#type' => 'radios',
      '#title' => $this->t('Sort order'),
      '#required' => TRUE,
      '#default_value' => $this->options['sort_order'],
      '#options' => [
        'asc' => $this->t('Ascending'),
        'desc' => $this->t('Descending'),
      ],
      '#weight' => 2,
    ];
  }

}
