# Year

Year extends NumericItemBase to provide a custom, integer-based field to
capture only the year part of a date.

- For a full description of the module, visit the
[project page](https://www.drupal.org/project/year).

- Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/year).


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Installation of Year Only module is similar to other contributed drupal modules.
For further information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

The module has no menu or modifiable settings. There is no configuration.


## Maintainers

- Michael Caldwell - [justcaldwell](https://www.drupal.org/u/justcaldwell)
